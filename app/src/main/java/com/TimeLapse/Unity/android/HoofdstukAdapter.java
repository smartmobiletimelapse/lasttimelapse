package com.TimeLapse.Unity.android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.TimeLapse.Unity.R;

import java.util.List;

/**
 * Created by Joey on 31-3-2016.
 */
public class HoofdstukAdapter extends ArrayAdapter<Hoofdstuk> {

    private Context context;
    private List<Hoofdstuk> hoofdstukken;

    public HoofdstukAdapter(Context context, List<Hoofdstuk> hoofdstukken) {
        super(context, R.layout.hoofdstukitem, hoofdstukken);

        this.context = context;
        this.hoofdstukken = hoofdstukken;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Hoofdstuk requestedHoofdstuk = hoofdstukken.get(position);

        View hoofdstukView = convertView;

        if(hoofdstukView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            hoofdstukView = inflater.inflate(R.layout.hoofdstukitem, null);
        }


        TextView titelView = (TextView) hoofdstukView.findViewById(R.id.hoofdstukTitel);
        titelView.setText(requestedHoofdstuk.hoofdstukTitel);


        return hoofdstukView;
    }
}
