package com.TimeLapse.Unity.android;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import com.TimeLapse.Unity.R;
import com.google.unity.GoogleUnityActivity;

import java.util.List;



/**
 * Created by Joey on 31-3-2016.
 */
public class Hoofdstukken extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hoofdstukken);
        TijdvakProvider tijdvakProvider = new TijdvakProvider(this);
        final List<Tijdvak> tijdvakken = tijdvakProvider.getTijdVakList();

        final int chosenTijdvak = getIntent().getExtras().getInt("Tijdvak");
        Tijdvak tijdvak = tijdvakken.get(chosenTijdvak);



        Window window = getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        float[] hsv = new float[3];
        int color = getResources().getColor(tijdvak.tijdvakColor);
        Color.colorToHSV(color, hsv);
        hsv[2] *= 0.8f; // value component
        color = Color.HSVToColor(hsv);

        window.setStatusBarColor(color);





        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setBackgroundColor(getResources().getColor(tijdvak.tijdvakColor));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);



        setTitle(tijdvak.tijdvakTitel);

        HoofdstukProvider hoofdstukProvider = new HoofdstukProvider(this);
        hoofdstukProvider.fillhoofdstukList(tijdvak.Hoofdstukken, tijdvak.index);

        HoofdstukAdapter hoofdstukAdapter = new HoofdstukAdapter(this, HoofdstukProvider.getHoofdstukList());

        ListView listview = (ListView) findViewById(R.id.Hoofdstukview);
        listview.setAdapter(hoofdstukAdapter);


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view2);
        navigationView.setNavigationItemSelectedListener(this);




        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String ActivityID = (tijdvakken.get(chosenTijdvak).index+1)+""+(position+1);
                Log.e("ActivityID ", ActivityID);
                Intent intent;
                switch(ActivityID){
                    case "11": intent = new Intent(view.getContext(), GoogleUnityActivity.class);
                        break;
                    default: intent = null;


                }

                if(intent != null) {
                    startActivity(intent);
                }
            }
        });


    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
