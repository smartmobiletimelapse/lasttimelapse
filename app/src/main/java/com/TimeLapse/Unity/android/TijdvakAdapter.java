package com.TimeLapse.Unity.android;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.TimeLapse.Unity.R;

import java.util.List;

/**
 * Created by Joey on 31-3-2016.
 */
public class TijdvakAdapter extends ArrayAdapter<Tijdvak> {

    private Context context;
    private List<Tijdvak> tijdvakken;

    public TijdvakAdapter(Context context, List<Tijdvak> tijdvakken) {
        super(context, R.layout.tijdvakitem, tijdvakken);

        this.context = context;
        this.tijdvakken = tijdvakken;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Tijdvak requestedTijdvak = tijdvakken.get(position);

        View tijdvakView = convertView;

        if(tijdvakView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            tijdvakView = inflater.inflate(R.layout.tijdvakitem, null);
        }

        ImageView pictureView = (ImageView) tijdvakView.findViewById(R.id.tijdvakAfbeelding);
        Drawable tijdvakAfbeelding = context.getResources().getDrawable(requestedTijdvak.tijdvakAfbeelding);
        pictureView.setImageDrawable(tijdvakAfbeelding);

        TextView titelView = (TextView) tijdvakView.findViewById(R.id.tijdvakTitel);
        titelView.setText(requestedTijdvak.tijdvakTitel);

        titelView.setBackgroundColor(context.getResources().getColor(requestedTijdvak.tijdvakColor));

        return tijdvakView;
    }
}
