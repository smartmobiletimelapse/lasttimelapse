package com.TimeLapse.Unity.android;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joey on 31-3-2016.
 */
public class HoofdstukProvider {


    private static List<Hoofdstuk> hoofdstukList;
    private Context context;





    public HoofdstukProvider(Context context){
        this.context = context;

        if(hoofdstukList == null)
        {
            hoofdstukList = new ArrayList<Hoofdstuk>();

        }
    }

    public static Hoofdstuk GetHoofdstuk(int index) {
        if(index >= hoofdstukList.size()) return null;
        return hoofdstukList.get(index);
    }


    public static List<Hoofdstuk> getHoofdstukList()
    {

        return hoofdstukList;
    }

    public List<Hoofdstuk> fillhoofdstukList(int id, int index) {

        String[] hoofdstukken = context.getResources().getStringArray(id);
        hoofdstukList.clear();



        for (int hoofdstukIndex = 0; hoofdstukIndex < hoofdstukken.length; hoofdstukIndex++) {
            Hoofdstuk hoofdstuk = new Hoofdstuk();

            hoofdstuk.hoofdstukTitel = hoofdstukken[hoofdstukIndex];
            hoofdstuk.activity = index+""+hoofdstukIndex;

            hoofdstukList.add(hoofdstuk);

        }
        return hoofdstukList;
    }
}
