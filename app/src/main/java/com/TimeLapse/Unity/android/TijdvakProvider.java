package com.TimeLapse.Unity.android;

import android.content.Context;

import com.TimeLapse.Unity.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joey on 31-3-2016.
 */
public class TijdvakProvider {


    private static List<Tijdvak> tijdvakList;
    private Context context;




    public TijdvakProvider(Context context){
        this.context = context;

        if(tijdvakList == null)
        {
            tijdvakList = new ArrayList<Tijdvak>();
            tijdvakList = filltijdvakList();
        }
    }

    public static Tijdvak GetTijdvak(int index) {
        if(index >= tijdvakList.size()) return null;
        return tijdvakList.get(index);
    }

    public List<Tijdvak> getTijdVakList()
    {

        return tijdvakList;
    }

    private List<Tijdvak> filltijdvakList() {
        String[] tijdvakTitel = context.getResources().getStringArray(R.array.tijdvakTitel);
        String[] tijdvakAfbeelding = context.getResources().getStringArray(R.array.tijdvakIcon);




        for (int tijdvakIndex = 0; tijdvakIndex < tijdvakTitel.length; tijdvakIndex++) {
            Tijdvak tijdvak = new Tijdvak();

            tijdvak.tijdvakTitel = tijdvakTitel[tijdvakIndex];
            int id = context.getResources().getIdentifier(tijdvakAfbeelding[tijdvakIndex], "drawable", context.getPackageName());

            int tijdvakColor = context.getResources().getIdentifier(tijdvakAfbeelding[tijdvakIndex], "color", context.getPackageName());

            int hoofdstuklijst = context.getResources().getIdentifier(tijdvakAfbeelding[tijdvakIndex], "array", context.getPackageName());


            tijdvak.tijdvakAfbeelding = id;

            tijdvak.tijdvakColor = tijdvakColor;

            tijdvak.Hoofdstukken = hoofdstuklijst;

            tijdvak.index = tijdvakIndex;

            tijdvakList.add(tijdvak);

        }
        return tijdvakList;
    }
}
